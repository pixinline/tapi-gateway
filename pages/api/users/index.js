import admin from '../../../firebase/admin';
import firebase from '../../../firebase/firebase';
import authenticated from '../../../utils/auth/authenticated';
import {
    ROLE_ADMIN,
    ROLE_MANAGER,
    ROLE_SUPER_ADMIN,
    ROLE_USER,
} from '../../../utils/constants';

export default authenticated(async function users(req, res) {
    const { role } = res.locals;
    if (req.method === 'GET') {
        if (
            !role ||
            ![ROLE_SUPER_ADMIN, ROLE_ADMIN, ROLE_MANAGER].includes(role)
        ) {
            return res.status(403).json({ message: 'Forbidden' });
        }
        try {
            const listUsersResult = await admin.auth().listUsers();
            const filteredUsers = [];
            listUsersResult.users.map((user) => {
                if (user.customClaims && user.customClaims.role) {
                    if (role === ROLE_SUPER_ADMIN) {
                        filteredUsers.push(decodeUser(user));
                    } else if (
                        role === ROLE_ADMIN &&
                        (user.customClaims.role === ROLE_ADMIN ||
                            user.customClaims.role === ROLE_MANAGER ||
                            user.customClaims.role === ROLE_USER)
                    ) {
                        filteredUsers.push(decodeUser(user));
                    } else if (
                        role === ROLE_MANAGER &&
                        (user.customClaims.role === ROLE_MANAGER ||
                            user.customClaims.role === ROLE_USER)
                    ) {
                        filteredUsers.push(decodeUser(user));
                    }
                }
            });
            return res.status(200).send({ data: filteredUsers });
        } catch (error) {
            console.error(error);
            return res.status(500).json({
                message: 'Internal Server Error',
            });
        }
    } else if (req.method === 'POST') {
        if (
            !role ||
            ![ROLE_SUPER_ADMIN, ROLE_ADMIN, ROLE_MANAGER].includes(role)
        ) {
            return res.status(403).json({ message: 'Forbidden' });
        }
        const newUser = {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: req.body.password,
            role: req.body.role,
        };
        if (
            role === ROLE_SUPER_ADMIN ||
            (role === ROLE_ADMIN &&
                (newUser.role === ROLE_ADMIN ||
                    newUser.role === ROLE_MANAGER ||
                    newUser.role === ROLE_USER)) ||
            (role === ROLE_MANAGER &&
                (newUser.role === ROLE_MANAGER || newUser.role === ROLE_USER))
        ) {
            try {
                const userCredential = await firebase
                    .auth()
                    .createUserWithEmailAndPassword(
                        newUser.email,
                        newUser.password
                    );
                admin.auth().setCustomUserClaims(userCredential.user.uid, {
                    role: newUser.role,
                });
                userCredential.user.sendEmailVerification();
                return res.status(201).send({ uid: userCredential.user.uid });
            } catch (error) {
                console.error(error);
                if (error.errorInfo) {
                    return res
                        .status(400)
                        .json({ message: error.errorInfo.message });
                } else {
                    return res
                        .status(500)
                        .json({ message: 'Internal Server Error' });
                }
            }
        } else {
            return res.status(403).send({ message: 'Forbidden' });
        }
    } else {
        return res
            .status(405)
            .json({ message: `Method ${req.method} Not Allowed` });
    }
});

function decodeUser(firebaseUser) {
    return {
        email: firebaseUser.email,
        emailVerified: firebaseUser.emailVerified,
        disabled: firebaseUser.disabled,
        creationTime: firebaseUser.metadata.creationTime,
        lastSignInTime: firebaseUser.metadata.lastSignInTime,
        role: firebaseUser.customClaims.role,
    };
}
