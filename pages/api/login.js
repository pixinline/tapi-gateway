import firebase from '../../firebase/firebase';
import { ROLE_SUPER_ADMIN } from '../../utils/constants';
import { validateLoginData } from '../../utils/validators';
import cookie from 'cookie';

export default async function login(req, res) {
    if (req.method !== 'POST') {
        return res
            .status(405)
            .json({ message: `Method ${req.method} Not Allowed` });
    }

    const user = {
        email: req.body.email,
        password: req.body.password,
    };

    const { valid, errors } = validateLoginData(user);
    if (!valid) return res.status(400).json(errors);

    try {
        const userCredential = await firebase
            .auth()
            .signInWithEmailAndPassword(user.email, user.password);
        const idTokenResult = await userCredential.user.getIdTokenResult(true);
        if (
            idTokenResult.claims.email_verified ||
            idTokenResult.claims.role === ROLE_SUPER_ADMIN
        ) {
            res.setHeader(
                'Set-Cookie',
                cookie.serialize('auth', idTokenResult.token, {
                    //httpOnly: true,
                    secure: process.env.NODE_ENV !== 'development',
                    sameSite: 'strict',
                    maxAge: 3600,
                    path: '/',
                })
            );
            return res.status(200).json({
                message: 'OK',
                data: {
                    role: idTokenResult.claims.role,
                },
            });
        } else {
            return res.status(200).json({
                error: 'The email is not verified.',
            });
        }
    } catch (error) {
        console.error(error);
        return res.status(403).json({ message: 'Invalid credentials.' });
    }
}
