import nc from 'next-connect';
import admin from '../../../firebase/admin';
import firebase from '../../../firebase/firebase';
import isAuthenticated from '../../../utils/auth/authenticated';
import {
    ROLE_ADMIN,
    ROLE_MANAGER,
    ROLE_SUPER_ADMIN,
    ROLE_USER,
} from '../../../utils/constants';
import 'firebase/auth';

function onNoMatch(req, res) {
    res.status(405).json({ message: `Method ${req.method} Not Allowed` });
}

const handler = nc({ onNoMatch }).use(isAuthenticated);

handler.get((_, res) => {
    const { role } = res.locals;

    if (!role || ![ROLE_SUPER_ADMIN, ROLE_ADMIN, ROLE_MANAGER].includes(role)) {
        return res.status(403).json({ message: 'Forbidden' });
    }

    admin
        .auth()
        .listUsers()
        .then((result) => {
            const filteredUsers = [];
            result.users.map((user) => {
                if (user.customClaims && user.customClaims.role) {
                    if (role === ROLE_SUPER_ADMIN) {
                        filteredUsers.push(decodeUser(user));
                    } else if (
                        role === ROLE_ADMIN &&
                        (user.customClaims.role === ROLE_ADMIN ||
                            user.customClaims.role === ROLE_MANAGER ||
                            user.customClaims.role === ROLE_USER)
                    ) {
                        filteredUsers.push(decodeUser(user));
                    } else if (
                        role === ROLE_MANAGER &&
                        (user.customClaims.role === ROLE_MANAGER ||
                            user.customClaims.role === ROLE_USER)
                    ) {
                        filteredUsers.push(decodeUser(user));
                    }
                }
            });
            return res.status(200).send(filteredUsers);
        })
        .catch((error) => {
            console.error(error);
            return res.status(500).json({
                message: 'Internal Server Error',
            });
        });
});

handler.post(async (req, res) => {
    const { role } = res.locals;

    if (!role || ![ROLE_SUPER_ADMIN, ROLE_ADMIN, ROLE_MANAGER].includes(role)) {
        return res.status(403).json({ message: 'Forbidden' });
    }

    const newUser = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password,
        role: req.body.role,
    };
    if (
        role === ROLE_SUPER_ADMIN ||
        (role === ROLE_ADMIN &&
            (newUser.role === ROLE_ADMIN ||
                newUser.role === ROLE_MANAGER ||
                newUser.role === ROLE_USER)) ||
        (role === ROLE_MANAGER &&
            (newUser.role === ROLE_MANAGER || newUser.role === ROLE_USER))
    ) {
        firebase
            .auth()
            .createUserWithEmailAndPassword(newUser.email, newUser.password)
            .then((userCredential) => {
                admin.auth().setCustomUserClaims(userCredential.user.uid, {
                    role: newUser.role,
                });
                userCredential.user.sendEmailVerification();
                return res.status(201).send({ uid: userCredential.user.uid });
            })
            .catch((error) => {
                console.error(error);
                if (error.errorInfo) {
                    return response
                        .status(400)
                        .json({ message: error.errorInfo.message });
                } else {
                    return response
                        .status(500)
                        .json({ message: 'Internal Server Error' });
                }
            });
    } else {
        return res.status(403).send({ message: 'Forbidden' });
    }
});

function decodeUser(firebaseUser) {
    return {
        email: firebaseUser.email,
        emailVerified: firebaseUser.emailVerified,
        disabled: firebaseUser.disabled,
        creationTime: firebaseUser.metadata.creationTime,
        lastSignInTime: firebaseUser.metadata.lastSignInTime,
        role: firebaseUser.customClaims.role,
    };
}

export default handler;
