import Base from '../components/Layout/Base';
import '../utils/i18n';
//import '../styles/globals.css';
//import '../polyfills.js';
import 'whirl/dist/whirl.css';
import '@fortawesome/fontawesome-free/css/brands.css';
import '@fortawesome/fontawesome-free/css/regular.css';
import '@fortawesome/fontawesome-free/css/solid.css';
import '@fortawesome/fontawesome-free/css/fontawesome.css';
import 'animate.css/animate.min.css';
import 'simple-line-icons/css/simple-line-icons.css';
import '../styles/bootstrap.scss';
import '../styles/app.scss';
import '../styles/themes/theme-b.scss';

function App({ Component, pageProps }) {
    const Layout = Component.Layout ? Component.Layout : Base;

    return (
        <Layout>
            <Component {...pageProps} />
        </Layout>
    );
}

export default App;
