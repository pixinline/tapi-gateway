import { useState } from 'react';
import BasePage from '../components/Layout/BasePage';
import Link from 'next/link';
import { ToastContainer, toast } from 'react-toastify';
import { Input, CustomInput } from 'reactstrap';
import FormValidator from '../components/Forms/Validator';
import 'react-toastify/dist/ReactToastify.css';
import { useRouter } from 'next/router';

const Login = () => {
    const router = useRouter();
    const [state, setState] = useState({
        formLogin: {
            email: '',
            password: '',
        },
    });

    const validateOnChange = (event) => {
        const input = event.target;
        const form = input.form;
        const value = input.type === 'checkbox' ? input.checked : input.value;
        const result = FormValidator.validate(input);

        setState({
            [form.name]: {
                ...state[form.name],
                [input.name]: value,
                errors: {
                    ...state[form.name].errors,
                    [input.name]: result,
                },
            },
        });
    };

    const hasError = (formName, inputName, method) => {
        return (
            state[formName] &&
            state[formName].errors &&
            state[formName].errors[inputName] &&
            state[formName].errors[inputName][method]
        );
    };

    const onSubmit = (e) => {
        e.preventDefault();
        const form = e.target;
        const inputs = [...form.elements].filter((i) =>
            ['INPUT', 'SELECT'].includes(i.nodeName)
        );
        const { errors } = FormValidator.bulkValidate(inputs);
        setState({
            [form.name]: {
                ...state[form.name],
                errors,
            },
        });
        handleLogin();
    };

    async function handleLogin() {
        const resp = await fetch('/api/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: state.formLogin.email,
                password: state.formLogin.password,
            }),
        });
        if (resp.ok) {
            const json = await resp.json();
            if (json.error) {
                toast(json.error, {
                    type: 'error',
                    position: 'top-right',
                });
            } else {
                router.push('/users');
            }
        } else {
            const json = await resp.json();
            toast(json.message, {
                type: 'error',
                position: 'top-right',
            });
        }
    }

    return (
        <div className="block-center mt-4 wd-xl">
            <div className="card card-flat">
                <div className="card-header text-center bg-dark">
                    <a href="">
                        <img
                            className="block-center rounded"
                            src="img/logo.png"
                            alt="Logo"
                        />
                    </a>
                </div>
                <div className="card-body">
                    <p className="text-center py-2">SIGN IN TO CONTINUE.</p>
                    <form className="mb-3" name="formLogin" onSubmit={onSubmit}>
                        <div className="form-group">
                            <div className="input-group with-focus">
                                <Input
                                    type="email"
                                    name="email"
                                    className="border-right-0"
                                    placeholder="Enter email"
                                    invalid={
                                        hasError(
                                            'formLogin',
                                            'email',
                                            'required'
                                        ) ||
                                        hasError('formLogin', 'email', 'email')
                                    }
                                    onChange={validateOnChange}
                                    data-validate='["required", "email"]'
                                    value={state.formLogin.email}
                                />
                                <div className="input-group-append">
                                    <span className="input-group-text text-muted bg-transparent border-left-0">
                                        <em className="fa fa-envelope"></em>
                                    </span>
                                </div>
                                {hasError('formLogin', 'email', 'required') && (
                                    <span className="invalid-feedback">
                                        Field is required
                                    </span>
                                )}
                                {hasError('formLogin', 'email', 'email') && (
                                    <span className="invalid-feedback">
                                        Field must be valid email
                                    </span>
                                )}
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="input-group with-focus">
                                <Input
                                    type="password"
                                    id="id-password"
                                    name="password"
                                    className="border-right-0"
                                    placeholder="Password"
                                    invalid={hasError(
                                        'formLogin',
                                        'password',
                                        'required'
                                    )}
                                    onChange={validateOnChange}
                                    data-validate='["required"]'
                                    value={state.formLogin.password}
                                />
                                <div className="input-group-append">
                                    <span className="input-group-text text-muted bg-transparent border-left-0">
                                        <em className="fa fa-lock"></em>
                                    </span>
                                </div>
                                <span className="invalid-feedback">
                                    Field is required
                                </span>
                            </div>
                        </div>
                        <div className="clearfix">
                            <CustomInput
                                type="checkbox"
                                id="rememberme"
                                className="float-left mt-0"
                                name="remember"
                                label="Remember Me"
                            ></CustomInput>
                            <div className="float-right">
                                <Link href="/pages/recover" as="/recover">
                                    <a className="text-muted">
                                        Forgot your password?
                                    </a>
                                </Link>
                            </div>
                        </div>
                        <button
                            className="btn btn-block btn-primary mt-3"
                            type="submit"
                        >
                            Login
                        </button>
                        <ToastContainer />
                    </form>
                </div>
            </div>
            <div className="p-3 text-center">
                <span className="mr-2">&copy;</span>
                <span>2020</span>
                <span className="mx-2">-</span>
                <span>Tapi</span>
                <br />
                <span>All rights reserved</span>
            </div>
        </div>
    );
};

Login.Layout = BasePage;

export default Login;
