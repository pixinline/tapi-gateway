import { useTranslation } from 'react-i18next';
import ContentWrapper from '../components/Layout/ContentWrapper';
import {
    Modal,
    ModalHeader,
    Card,
    ModalBody,
    ModalFooter,
    Table,
    Button,
} from 'reactstrap';
import { useState } from 'react';
import NewUserForm from '../components/Forms/NewUserForm';
import fetch from 'isomorphic-unfetch';
import Router from 'next/router';

const Users = ({ users = [] }) => {
    const { t } = useTranslation();
    const [state, setState] = useState({ modal: false });

    const toggleModal = () => {
        setState({
            modal: !state.modal,
        });
    };

    return (
        <ContentWrapper>
            <div className="content-heading">
                <div>
                    {t('users')}
                    <small>{t('users-description')}</small>
                </div>
                <div className="ml-auto">
                    <Button color="primary" size="sm" onClick={toggleModal}>
                        Create new user
                    </Button>
                    <Modal centered isOpen={state.modal}>
                        <ModalHeader>New user</ModalHeader>
                        <ModalBody>
                            <NewUserForm />
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary" onClick={toggleModal}>
                                Create
                            </Button>{' '}
                            <Button color="secondary" onClick={toggleModal}>
                                Cancel
                            </Button>
                        </ModalFooter>
                    </Modal>
                </div>
            </div>
            <Card className="card-default">
                {/*<CardHeader>Demo Table #4</CardHeader>*/}
                {/* START table-responsive */}
                <Table striped bordered hover responsive>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Company</th>
                            <th>Role</th>
                            <th>Email</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {users.map((user) => (
                            <tr key={user.email}>
                                <td>Name</td>
                                <td>Company</td>
                                <td>
                                    <div className="badge badge-primary">
                                        {user.role}
                                    </div>
                                </td>
                                <td>
                                    {user.email}
                                    <br />
                                    {user.emailVerified ? (
                                        <div className="badge badge-success">
                                            Verified
                                        </div>
                                    ) : (
                                        <div className="badge badge-warning">
                                            Not verified
                                        </div>
                                    )}
                                </td>
                                <td>
                                    {user.disabled ? (
                                        <div className="badge badge-secondary">
                                            Disabled
                                        </div>
                                    ) : (
                                        <div className="badge badge-success">
                                            Enabled
                                        </div>
                                    )}
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
                {/* END table-responsive */}
            </Card>
        </ContentWrapper>
    );
};

Users.getInitialProps = async (ctx) => {
    var cookie = '';
    if (ctx.req) {
        cookie = ctx.req.headers.cookie;
    }
    const resp = await fetch('http://localhost:3000/api/users', {
        headers: {
            cookie: cookie,
        },
    });
    if (resp.status === 401 && !ctx.req) {
        Router.replace('/login');
    }
    if (resp.status === 401 && ctx.req) {
        ctx.res.writeHead(302, {
            Location: '/login',
        });
        ctx.res.end();
    }
    const json = await resp.json();
    return { users: json.data };
};

export default Users;
