import ContentWrapper from '../components/Layout/ContentWrapper';
import { useTranslation } from 'react-i18next';
import Base from '../components/Layout/Base';

const Dashboard = ({ users }) => {
    const { t } = useTranslation();

    return (
        <ContentWrapper>
            <div className="content-heading">
                <div>
                    Dashboard
                    <small>{t('welcome-to-tapi-gateway')}</small>
                </div>
                {/*<div className="ml-auto">
                    <Dropdown
                        isOpen={state.dropdownTranslateOpen}
                        toggle={toggleDDTranslate}
                    >
                        <DropdownToggle>English</DropdownToggle>
                        <DropdownMenu className="dropdown-menu-right-forced animated fadeInUpShort">
                            <DropdownItem onClick={() => changeLanguage('en')}>
                                English
                            </DropdownItem>
                            <DropdownItem onClick={() => changeLanguage('es')}>
                                Spanish
                            </DropdownItem>
                        </DropdownMenu>
                    </Dropdown>
                </div>*/}
            </div>
        </ContentWrapper>
    );
};

Dashboard.getInitialProps = async (ctx) => {
    var cookie = '';
    if (ctx.req) {
        cookie = ctx.req.headers.cookie;
    }
    const resp = await fetch('http://localhost:3000/api/users', {
        headers: {
            cookie: cookie,
        },
    });
    if (resp.status === 401 && !ctx.req) {
        Router.replace('/login');
    }
    if (resp.status === 401 && ctx.req) {
        ctx.res.writeHead(302, {
            Location: '/login',
        });
        ctx.res.end();
    }
    const json = await resp.json();
    return { users: json.data };
};

export default Dashboard;
