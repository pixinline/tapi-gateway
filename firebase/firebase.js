import firebase from 'firebase/app';
import config from './config';
import 'firebase/auth';

if (!firebase.apps.length) {
    firebase.initializeApp(config);
}

export default firebase;
