import { useState } from 'react';
import {
    Row,
    Col,
    Input,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    CustomInput,
} from 'reactstrap';
import FormValidator from './Validator';

const NewUserForm = () => {
    const [state, setState] = useState({
        formRegister: {
            firstName: '',
            lastName: '',
            email: '',
            role: '',
            company: '',
            password: '',
            password2: '',
            terms: false,
        },
    });

    const validateOnChange = (event) => {
        const input = event.target;
        const form = input.form;
        const value = input.type === 'checkbox' ? input.checked : input.value;
        const result = FormValidator.validate(input);
        setState({
            [form.name]: {
                ...state[form.name],
                [input.name]: value,
                errors: {
                    ...state[form.name].errors,
                    [input.name]: result,
                },
            },
        });
    };

    const hasError = (formName, inputName, method) => {
        return (
            state[formName] &&
            state[formName].errors &&
            state[formName].errors[inputName] &&
            state[formName].errors[inputName][method]
        );
    };

    const onSubmit = (e) => {
        e.preventDefault();
    };

    return (
        <form className="mb-3" name="formRegister" onSubmit={onSubmit}>
            <Row>
                <Col>
                    <div className="form-group">
                        <label className="col-form-label">Name *</label>
                        <Input
                            type="text"
                            name="firstName"
                            invalid={hasError('formRegister', 'required')}
                            onChange={validateOnChange}
                            data-validate='["required"]'
                            value={state.formRegister.firstName}
                        />
                        {hasError('formRegister', 'required') && (
                            <span className="invalid-feedback">
                                Field is required
                            </span>
                        )}
                    </div>
                </Col>
                <Col>
                    <div className="form-group">
                        <label className="col-form-label">Last Name *</label>
                        <Input
                            type="text"
                            name="lastName"
                            invalid={hasError('formRegister', 'required')}
                            onChange={validateOnChange}
                            data-validate='["required"]'
                            value={state.formRegister.lastName}
                        />
                        {hasError('formRegister', 'required') && (
                            <span className="invalid-feedback">
                                Field is required
                            </span>
                        )}
                    </div>
                </Col>
            </Row>
            <div className="form-group">
                <label className="col-form-label">Email Address *</label>
                <Input
                    type="email"
                    name="email"
                    invalid={
                        hasError('formRegister', 'email', 'required') ||
                        hasError('formRegister', 'email', 'email')
                    }
                    onChange={validateOnChange}
                    data-validate='["required", "email"]'
                    value={state.formRegister.email}
                />
                {hasError('formRegister', 'email', 'required') && (
                    <span className="invalid-feedback">Field is required</span>
                )}
                {hasError('formRegister', 'email', 'email') && (
                    <span className="invalid-feedback">
                        Field must be valid email
                    </span>
                )}
            </div>
            <div className="form-group">
                <label className="col-form-label">Role *</label>
                <select
                    name="role"
                    defaultValue=""
                    className="custom-select custom-select-sm"
                    onChange={validateOnChange}
                >
                    <option>Open this select menu</option>
                    <option defaultValue="1">One</option>
                    <option defaultValue="2">Two</option>
                    <option defaultValue="3">Three</option>
                </select>
                {hasError('formRegister', 'required') && (
                    <span className="invalid-feedback">Field is required</span>
                )}
            </div>
            <div className="form-group">
                <label className="col-form-label">Company *</label>
                <Input
                    type="text"
                    name="company"
                    invalid={hasError('formRegister', 'required')}
                    onChange={validateOnChange}
                    data-validate='["required"]'
                    value={state.formRegister.company}
                />
                {hasError('formRegister', 'required') && (
                    <span className="invalid-feedback">Field is required</span>
                )}
            </div>
            <div className="form-group">
                <label className="col-form-label">Password *</label>
                <Input
                    type="text"
                    id="id-password1"
                    name="password"
                    invalid={hasError('formRegister', 'password', 'required')}
                    onChange={validateOnChange}
                    data-validate='["required"]'
                    value={state.formRegister.password}
                />
                <span className="invalid-feedback">Field is required</span>
            </div>
            <div className="form-group">
                <label className="col-form-label">Confirm Password *</label>
                <Input
                    type="text"
                    name="password2"
                    invalid={hasError('formRegister', 'password2', 'equalto')}
                    onChange={validateOnChange}
                    data-validate='["equalto"]'
                    value={state.formRegister.password2}
                    data-param="id-password1"
                />
                <span className="invalid-feedback">
                    Field must be equal to previous
                </span>
            </div>
            <div className="required">* Required fields</div>
        </form>
    );
};

export default NewUserForm;
