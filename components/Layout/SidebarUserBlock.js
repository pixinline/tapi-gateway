const SidebarUserBlock = () => {
    return (
        <div>
            <div className="item user-block">
                {/* User picture */}
                <div className="user-block-picture">
                    <div className="user-block-status">
                        <img
                            className="img-thumbnail rounded-circle"
                            src="/img/dummy.png"
                            alt="Avatar"
                            width="60"
                            height="60"
                        />
                    </div>
                </div>
                {/* Name and Job */}
                <div className="user-block-info">
                    <span className="user-block-name">Name Lastname</span>
                    <span className="user-block-role">Company</span>
                </div>
            </div>
        </div>
    );
};

export default SidebarUserBlock;
