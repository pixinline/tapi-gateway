import Head from './Head';
import Header from './Header';
import ThemeProvider from './ThemeProvider';
import Sidebar from './Sidebar';
import SettingsProvider from './SettingsProvider';
import Footer from './Footer';

const Base = (props) => {
    return (
        <ThemeProvider>
            <SettingsProvider>
                <div className="wrapper">
                    <Head />
                    <Header />
                    <Sidebar />
                    {/*<Offsidebar />*/}
                    <section className="section-container">
                        {props.children}
                    </section>
                    <Footer />
                </div>
            </SettingsProvider>
        </ThemeProvider>
    );
};

export default Base;
