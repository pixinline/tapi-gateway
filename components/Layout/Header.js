import { useContext } from 'react';
import { SettingsContext } from './SettingsProvider';
import cookies from 'js-cookie';
import { useRouter } from 'next/router';

const Header = () => {
    const { toggleSetting } = useContext(SettingsContext);
    const router = useRouter();

    const logout = (e) => {
        e.preventDefault();
        cookies.remove('auth');
        router.push('/login');
    };

    const toggleCollapsed = (e) => {
        e.preventDefault();
        toggleSetting('isCollapsed');
        resize();
    };

    const toggleAside = (e) => {
        e.preventDefault();
        toggleSetting('asideToggled');
    };

    function resize() {
        var evt = document.createEvent('UIEvents');
        evt.initUIEvent('resize', true, false, window, 0);
        window.dispatchEvent(evt);
    }

    return (
        <header className="topnavbar-wrapper">
            {/* START Top Navbar */}
            <nav className="navbar topnavbar">
                {/* START navbar header */}
                <div className="navbar-header">
                    <a className="navbar-brand" href="#/">
                        <div className="brand-logo">
                            <img
                                className="img-fluid"
                                src="img/logo.png"
                                alt="App Logo"
                            />
                        </div>
                        <div className="brand-logo-collapsed">
                            <img
                                className="img-fluid"
                                src="img/logo-single.jpg"
                                alt="App Logo"
                            />
                        </div>
                    </a>
                </div>
                {/* END navbar header */}

                {/* START Left navbar */}
                <ul className="navbar-nav mr-auto flex-row">
                    <li className="nav-item">
                        {/* Button used to collapse the left sidebar. Only visible on tablet and desktops */}
                        <a
                            href=""
                            className="nav-link d-none d-md-block d-lg-block d-xl-block"
                            onClick={toggleCollapsed}
                        >
                            <em className="fas fa-bars" />
                        </a>
                        {/* Button to show/hide the sidebar on mobile. Visible on mobile only. */}
                        <a
                            href=""
                            className="nav-link sidebar-toggle d-md-none"
                            onClick={toggleAside}
                        >
                            <em className="fas fa-bars" />
                        </a>
                    </li>
                </ul>
                {/* END Left navbar */}
                {/* START Right Navbar */}
                <ul className="navbar-nav flex-row">
                    {/* START Offsidebar button */}
                    <li className="nav-item">
                        <a className="nav-link" href="" onClick={logout}>
                            <em className="icon-logout" />
                        </a>
                    </li>
                    {/* END Offsidebar menu */}
                </ul>
                {/* END Right Navbar */}

                {/* START Search form */}
                {/*<HeaderSearch
                    isOpen={this.state.navSearchOpen}
                    onClose={this.closeNavSearch}
                />*/}
                {/* END Search form */}
            </nav>
            {/* END Top Navbar */}
        </header>
    );
};

export default Header;
