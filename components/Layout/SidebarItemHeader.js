import { useTranslation } from 'react-i18next';

const SidebarItemHeader = ({ item }) => {
    const { t } = useTranslation();

    return (
        <li className="nav-heading">
            <span>{t(item.translate)}</span>
        </li>
    );
};

export default SidebarItemHeader;
