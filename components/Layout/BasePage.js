import Head from './Head';

const BasePage = (props) => {
    return (
        <>
            <Head />
            <div className="wrapper">{props.children}</div>
        </>
    );
};

export default BasePage;
