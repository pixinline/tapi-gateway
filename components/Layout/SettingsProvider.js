import { useState, createContext } from 'react';

const SettingsContext = createContext();

const getClasses = (settings) => {
    let c = [];

    if (settings.isFixed) c.push('layout-fixed');
    if (settings.isBoxed) c.push('layout-boxed');
    if (settings.isCollapsed) c.push('aside-collapsed');
    if (settings.isCollapsedText) c.push('aside-collapsed-text');
    if (settings.isFloat) c.push('aside-float');
    if (settings.asideHover) c.push('aside-hover');
    if (settings.offsidebarOpen) c.push('offsidebar-open');
    if (settings.asideToggled) c.push('aside-toggled');
    // layout horizontal
    if (settings.horizontal) c.push('layout-h');

    return c.join(' ');
};

const SettingsProvider = (props) => {
    const [settings, setSettings] = useState({
        /* Layout fixed. Scroll content only */
        isFixed: true,
        /* Sidebar collapsed */
        isCollapsed: false,
        /* Boxed layout */
        isBoxed: false,
        /* Floating sidebar */
        isFloat: false,
        /* Sidebar show menu on hover only */
        asideHover: false,
        /* Show sidebar scrollbar (dont' hide it) */
        asideScrollbar: false,
        /* Sidebar collapsed with big icons and text */
        isCollapsedText: false,
        /* Toggle for the offsidebar */
        offsidebarOpen: false,
        /* Toggle for the sidebar offcanvas (mobile) */
        asideToggled: false,
        /* Toggle for the sidebar user block */
        showUserBlock: false,
        /* Enables layout horizontal */
        horizontal: false,
        /* Full size layout */
        useFullLayout: false,
        /* Hide footer */
        hiddenFooter: false,
    });

    const toggleSetting = (setting) => {
        const value = settings[setting];
        setSettings({
            ...settings,
            [setting]: !value,
        });
    };

    return (
        <SettingsContext.Provider
            value={{ settings: settings, toggleSetting: toggleSetting }}
        >
            <div id="__settings_provider" className={getClasses(settings)}>
                {props.children}
            </div>
        </SettingsContext.Provider>
    );
};

export { SettingsContext };
export default SettingsProvider;
