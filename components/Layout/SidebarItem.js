import Link from 'next/link';
import { Badge } from 'reactstrap';
import { useTranslation } from 'react-i18next';

const SidebarItem = ({ item, isActive, className, onMouseEnter }) => {
    const { t } = useTranslation();
    //debugger;

    return (
        <li className={isActive ? 'active' : ''} onMouseEnter={onMouseEnter}>
            <Link href={item.path} as={item.as}>
                <a title={item.name}>
                    {item.label && (
                        <Badge
                            tag="div"
                            className="float-right"
                            color={item.label.color}
                        >
                            {item.label.value}
                        </Badge>
                    )}
                    {item.icon && <em className={item.icon} />}
                    <span>{t(item.translate)}</span>
                </a>
            </Link>
        </li>
    );
};

export default SidebarItem;
