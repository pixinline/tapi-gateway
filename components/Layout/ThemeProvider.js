import '../../styles/themes/theme-a.scss';
import '../../styles/themes/theme-b.scss';
import '../../styles/themes/theme-c.scss';
import '../../styles/themes/theme-d.scss';
import '../../styles/themes/theme-e.scss';
import '../../styles/themes/theme-f.scss';
import '../../styles/themes/theme-h.scss';

const ThemeProvider = (props) => {
    return (
        <div id="__themes_provider" className={'theme-e'}>
            {props.children}
        </div>
    );
};

export default ThemeProvider;
