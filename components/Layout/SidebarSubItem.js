import { Badge, Collapse } from 'reactstrap';
import { useTranslation } from 'react-i18next';

const SidebarSubItem = ({
    item,
    isActive,
    handler,
    children,
    isOpen,
    onMouseEnter,
}) => {
    const { t } = useTranslation();

    return (
        <li className={isActive ? 'active' : ''}>
            <div
                className="nav-item"
                onClick={handler}
                onMouseEnter={onMouseEnter}
            >
                {item.label && (
                    <Badge
                        tag="div"
                        className="float-right"
                        color={item.label.color}
                    >
                        {item.label.value}
                    </Badge>
                )}
                {item.icon && <em className={item.icon} />}
                <span>{t(item.translate)}</span>
            </div>
            <Collapse isOpen={isOpen}>
                <ul id={item.path} className="sidebar-nav sidebar-subnav">
                    {children}
                </ul>
            </Collapse>
        </li>
    );
};

export default SidebarSubItem;
