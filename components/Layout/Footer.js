const Footer = () => {
    const year = new Date().getFullYear();

    return (
        <footer className="footer-container">
            <span>&copy; {year} - Tapi</span>
        </footer>
    );
};

export default Footer;
