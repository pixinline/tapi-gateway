import { useState, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import Router, { useRouter } from 'next/router';
import Menu from './Menu';
import { SettingsContext } from './SettingsProvider';
import SidebarItem from './SideBarItem';
import SidebarItemHeader from './SideBarItemHeader';
import SidebarSubItem from './SidebarSubItem';
import SidebarUserBlock from './SidebarUserBlock';

const parents = (element, selector) => {
    if (typeof selector !== 'string') {
        return null;
    }

    const parents = [];
    let ancestor = element.parentNode;

    while (
        ancestor &&
        ancestor.nodeType === Node.ELEMENT_NODE &&
        ancestor.nodeType !== 3 /*NODE_TEXT*/
    ) {
        if (ancestor.matches(selector)) {
            parents.push(ancestor);
        }

        ancestor = ancestor.parentNode;
    }
    return parents;
};

const outerHeight = (elem, includeMargin) => {
    const style = getComputedStyle(elem);
    const margins = includeMargin
        ? parseInt(style.marginTop, 10) + parseInt(style.marginBottom, 10)
        : 0;
    return elem.offsetHeight + margins;
};

const SidebarSubHeader = ({ item }) => (
    <li className="sidebar-subnav-header">{item.name}</li>
);

const SidebarBackdrop = ({ closeFloatingNav }) => (
    <div className="sidebar-backdrop" onClick={closeFloatingNav} />
);

const FloatingNav = ({
    item,
    target,
    routeActive,
    isFixed,
    closeFloatingNav,
}) => {
    let asideContainer = document.querySelector('.aside-container');
    let asideInner = asideContainer.firstElementChild; /*('.aside-inner')*/
    let sidebar = asideInner.firstElementChild; /*('.sidebar')*/

    let mar =
        parseInt(getComputedStyle(asideInner)['padding-top'], 0) +
        parseInt(getComputedStyle(asideContainer)['padding-top'], 0);
    let itemTop = target.parentElement.offsetTop + mar - sidebar.scrollTop;
    let vwHeight = document.body.clientHeight;

    const setPositionStyle = (el) => {
        if (!el) return;
        el.style.position = isFixed ? 'fixed' : 'absolute';
        el.style.top = itemTop + 'px';
        el.style.bottom =
            outerHeight(el, true) + itemTop > vwHeight ? 0 : 'auto';
    };

    return (
        <ul
            id={item.path}
            ref={setPositionStyle}
            className="sidebar-nav sidebar-subnav nav-floating"
            onMouseLeave={closeFloatingNav}
        >
            <SidebarSubHeader item={item} />
            {item.submenu.map((subitem, i) => (
                <SidebarItem
                    key={i}
                    item={subitem}
                    isActive={routeActive(subitem.path)}
                />
            ))}
        </ul>
    );
};

const Sidebar = (props) => {
    const { settings, toggleSetting } = useContext(SettingsContext);
    const router = useRouter();
    const [state, setState] = useState({
        collapse: {},
        showSidebarBackdrop: false,
        currentFloatingItem: null,
        currentFloatingItemTarget: null,
        pathname: router.pathname,
    });

    useEffect(() => {
        buildCollapseList();
        Router.events.on('routeChangeStart', handleRouteChange);
        Router.events.on('routeChangeComplete', handleRouteComplete);
        document.addEventListener('click', closeSidebarOnExternalClicks);
        return () => {
            document.removeEventListener('click', closeSidebarOnExternalClicks);
            Router.events.off('routeChangeStart', handleRouteChange);
            Router.events.off('routeChangeComplete', handleRouteComplete);
        };
    }, []);

    /** prepare initial state of collapse menus.*/
    const buildCollapseList = () => {
        let collapse = {};
        Menu.filter(({ heading }) => !heading).forEach(
            ({ name, path, submenu }) => {
                collapse[name] = routeActive(
                    submenu ? submenu.map(({ path }) => path) : path
                );
            }
        );
        setState({ ...state, collapse });
    };

    const handleRouteChange = () => {
        closeFloatingNav();
        closeSidebar();
    };

    const handleRouteComplete = (pathname) => {
        setState({
            ...state,
            pathname,
        });
    };

    const closeSidebar = () => {
        toggleSetting('asideToggled');
    };

    const closeSidebarOnExternalClicks = (e) => {
        // don't check if sidebar not visible
        if (!settings.asideToggled) return;
        if (
            !parents(e.target, '.aside-container').length && // if not child of sidebar
            !parents(e.target, '.topnavbar-wrapper').length && // if not child of header
            !e.target.matches('#user-block-toggle') && // user block toggle anchor
            !e.target.parentElement.matches('#user-block-toggle') // user block toggle icon
        ) {
            closeSidebar();
        }
    };

    const routeActive = (paths) => {
        const currpath = state.pathname;
        paths = Array.isArray(paths) ? paths : [paths];
        const isActive = paths.some((p) =>
            p === '/' ? currpath === p : currpath && currpath.indexOf(p) > -1
        );
        return isActive;
    };

    const toggleItemCollapse = (stateName) => () => {
        for (let c in state.collapse) {
            if (state.collapse[c] === true && c !== stateName)
                setState({
                    ...state,
                    collapse: {
                        [c]: false,
                    },
                });
        }
        setState({
            ...state,
            collapse: {
                [stateName]: !state.collapse[stateName],
            },
        });
    };

    const getSubRoutes = (item) => item.submenu.map(({ path }) => path);

    const itemType = (item) => {
        if (item.heading) return 'heading';
        if (!item.submenu) return 'menu';
        if (item.submenu) return 'submenu';
    };

    const shouldUseFloatingNav = () => {
        return (
            settings.isCollapsed ||
            settings.isCollapsedText ||
            settings.asideHover
        );
    };

    const showFloatingNav = (item) => (e) => {
        if (shouldUseFloatingNav())
            setState({
                ...state,
                currentFloatingItem: item,
                currentFloatingItemTarget: e.currentTarget,
                showSidebarBackdrop: true,
            });
    };

    const closeFloatingNav = () => {
        setState({
            ...state,
            currentFloatingItem: null,
            currentFloatingItemTarget: null,
            showSidebarBackdrop: false,
        });
    };

    return (
        <>
            <aside className="aside-container">
                {/* START Sidebar (left) */}
                <div className="aside-inner">
                    <nav className={'sidebar show-scrollbar'}>
                        {/* START sidebar nav */}
                        <ul className="sidebar-nav">
                            {/* START user info */}
                            <li className="has-user-block">
                                <SidebarUserBlock />
                            </li>
                            {/* END user info */}

                            {/* Iterates over all sidebar items */}
                            {Menu.map((item, i) => {
                                // heading
                                if (itemType(item) === 'heading')
                                    return (
                                        <SidebarItemHeader
                                            item={item}
                                            key={i}
                                        />
                                    );
                                else {
                                    if (itemType(item) === 'menu')
                                        return (
                                            <SidebarItem
                                                isActive={routeActive(
                                                    item.path
                                                )}
                                                item={item}
                                                key={i}
                                                onMouseEnter={closeFloatingNav}
                                            />
                                        );
                                    if (itemType(item) === 'submenu')
                                        return [
                                            <SidebarSubItem
                                                item={item}
                                                isOpen={
                                                    state.collapse[item.name]
                                                }
                                                handler={toggleItemCollapse(
                                                    item.name
                                                )}
                                                isActive={routeActive(
                                                    getSubRoutes(item)
                                                )}
                                                key={i}
                                                onMouseEnter={showFloatingNav(
                                                    item
                                                )}
                                            >
                                                <SidebarSubHeader
                                                    item={item}
                                                    key={i}
                                                />
                                                {item.submenu.map(
                                                    (subitem, i) => (
                                                        <SidebarItem
                                                            key={i}
                                                            item={subitem}
                                                            isActive={routeActive(
                                                                subitem.path
                                                            )}
                                                        />
                                                    )
                                                )}
                                            </SidebarSubItem>,
                                        ];
                                }
                                return null; // unrecognized item
                            })}
                        </ul>
                        {/* END sidebar nav */}
                    </nav>
                </div>
                {/* END Sidebar (left) */}
                {state.currentFloatingItem &&
                    state.currentFloatingItem.submenu && (
                        <FloatingNav
                            item={state.currentFloatingItem}
                            target={state.currentFloatingItemTarget}
                            routeActive={routeActive}
                            isFixed={props.settings.isFixed}
                            closeFloatingNav={closeFloatingNav}
                        />
                    )}
            </aside>
            {state.showSidebarBackdrop && (
                <SidebarBackdrop closeFloatingNav={closeFloatingNav} />
            )}
        </>
    );
};

Sidebar.propTypes = {
    actions: PropTypes.object,
    settings: PropTypes.object,
};

export default Sidebar;
