import * as admin from 'firebase-admin';

const authenticated = (fn) => async (req, res) => {
    const token = req.cookies.auth;
    if (!token) {
        return res.status(401).json({
            message: 'Unauthorized',
        });
    }
    try {
        const decodedToken = await admin.auth().verifyIdToken(token);
        res.locals = {
            ...res.locals,
            uid: decodedToken.uid,
            role: decodedToken.role,
            email: decodedToken.email,
        };
        return await fn(req, res);
    } catch (error) {
        console.error(error);
        return res.status(401).json({
            message: 'Unauthorized',
        });
    }
};

export default authenticated;
